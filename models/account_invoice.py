# -*- coding: utf-8 -*-
# © 2014 - 2017 Oxilia-Info (Jean-Christophe CHOQUET <jcchoquet@oxilia-info.fr>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

from odoo import models, fields, api, _

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    sale_subscription_id = fields.Many2one(
        'sale.subscription', string="Sale subscription")
