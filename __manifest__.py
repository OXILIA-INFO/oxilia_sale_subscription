# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Oxilia Sale Subscription',
    'summary': "An easy way to manage your client's subscriptions.",
    'installable': True,
    'application': True,
    'license' : 'AGPL-3',
    'depends': ['base', 'account', 'analytic', 'sale'],
    'data': [
        'data/sale_contract_data.xml',
        'security/ir.model.access.csv',
        'views/product_template_views.xml',
        'views/res_partner_views.xml',
        'views/sale_subscription_views.xml',
    ],
    'author': 'Oxilia-info',
    'description':
    """
        This module is used to trigger a recurrency client invoice :
            - rent
            - antivirus subscription
            - Any other regular payment that needs a recurrent invoice.
    """
}
