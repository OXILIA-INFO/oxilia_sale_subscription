Sale subscription
=====================

This project is a module for Odoo 11.0

Description
-----------

This module is used to manage client’s subscriptions:

- Create subscription such as rent, phone bill and internet subscription and all other regular payments that needs recurrent client’s invoices.
- Define how often you need to create invoices and the duration of the subscription.
- Invoices are automatically generated and already defined on the right period of time, depending on the previous invoice or the start date if it’s the first invoice.

Depends
-------

- ``base``
- ``account``
- ``analytic``
- ``sale``

--------------

Contact
-------

-  Homepage: http://oxilia-info.fr

